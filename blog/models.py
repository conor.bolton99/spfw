from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from django.core.validators import RegexValidator
from PIL import Image

# Create your models here.
class Post(models.Model):
    alphanumeric = RegexValidator(r'^[A-Za-z\d\s]*$', 'Only alphanumeric characters are allowed.')
    title = models.CharField(max_length=100, validators=[alphanumeric])
    content = models.TextField(validators=[alphanumeric])
    date_posted = models.DateTimeField(default = timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    likes = models.ManyToManyField(User, related_name='blog_post')


    def total_likes(self):
        return self.likes.count()
 
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse ('post-detail', kwargs={'pk':self.pk})

class Comment(models.Model):
    alphanumeric = RegexValidator(r'^[A-Za-z\d\s]*$', 'Only alphanumeric characters are allowed.')
    post = models.ForeignKey(Post, related_name="comments", on_delete=models.CASCADE)
    name = models.ForeignKey(User, on_delete=models.CASCADE)
    body = models.TextField(validators=[alphanumeric])
    date_added = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        ordering = ['-date_added']

    def __str__(self):
        return '%s %s' % (self.post.title, self.name)


class Gallery(models.Model):
    alphanumeric = RegexValidator(r'^[A-Za-z\d\s]*$', 'Only alphanumeric characters are allowed.')
    name = models.CharField(max_length=50, validators=[alphanumeric])
    gallery_Main_Img = models.ImageField(upload_to='images/')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        img = Image.open(self.gallery_Main_Img.path)

        if img.height > 300 or img.width > 300:
            output_size = (300,300)
            img.thumbnail(output_size)
            img.save(self.gallery_Main_Img.path)